/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import java.net.CookieHandler
import javax.inject.Inject
import javax.ws.rs.ApplicationPath
import javax.ws.rs.Path

@ApplicationPath("/testapp")
class TestRoutes @Inject constructor() {

    @Path("/hello")
    fun hello(): String = "hello"
}