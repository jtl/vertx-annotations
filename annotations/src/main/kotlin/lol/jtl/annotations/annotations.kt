/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package lol.jtl.annotations

import kotlin.reflect.KClass

const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"

val CharSequence.str: String
    get() = this.toString()

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class BindTo(val injectAs: KClass<*> = Unit::class, val className: String = "")

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class Order(val p: Int = 1)

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class Environment(val name: String = "")

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY)
@Retention(AnnotationRetention.SOURCE)
annotation class OnRoute(val code: String = "")