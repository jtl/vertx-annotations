import com.jfrog.bintray.gradle.BintrayExtension
import nl.javadude.gradle.plugins.license.LicenseExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    id("com.jfrog.bintray")
    `maven-publish`
}

dependencies {
    compile(kotlin("stdlib"))
    compile("io.vertx:vertx-lang-kotlin-coroutines:3.5.1")
    compile("com.squareup:kotlinpoet:0.7.0")
    compile("javax.ws.rs:jsr311-api:+")
    compile("com.google.dagger:dagger:2.+")
}
