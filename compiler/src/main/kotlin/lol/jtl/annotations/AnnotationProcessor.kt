/**
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package lol.jtl.annotations

import com.squareup.kotlinpoet.*
import dagger.Binds
import dagger.Component
import dagger.Module
import dagger.Provides
import java.io.File
import java.time.LocalDateTime
import java.util.*
import javax.annotation.processing.*
import javax.inject.Inject
import javax.inject.Singleton
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.type.MirroredTypeException
import javax.lang.model.type.TypeMirror
import javax.tools.Diagnostic
import javax.ws.rs.ApplicationPath
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes(
        "lol.jtl.annotations.Application",
        "lol.jtl.annotations.RouteHandler",
        "lol.jtl.annotations.BindTo",
        "dagger.Component",
        "javax.ws.rs.Path")
@SupportedOptions(KAPT_KOTLIN_GENERATED_OPTION_NAME)
class AnnotationProcessor : AbstractProcessor() {
    val pretty = false
    var coroutinesEnabled = false

    val buildTime = LocalDateTime.now()
    val port = listOf(System.getenv("BUILD_TIME_PORT"),
            "System.getenv(\"PORT\").let { if(it.isNullOrEmpty()) 9999 else it.toInt() }"
            ).find { !it.isNullOrBlank() }

    val handlers = mutableSetOf<Element>()
    val paths = mutableSetOf<Element>()
    val binded = mutableSetOf<Element>()
    val components = mutableSetOf<Element>()

    val p = { m: Any? ->
        processingEnv.messager.printMessage(Diagnostic.Kind.WARNING,
                (m?.toString() ?: "<null>"))
    }

    override fun process(annotations: MutableSet<out TypeElement>?, roundEnv: RoundEnvironment?): Boolean {

        val kaptKotlinGeneratedDir: String = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]?.str ?: run {
            processingEnv.messager.printMessage(Diagnostic.Kind.ERROR, "Can't find the target directory for generated Kotlin files.")
            return false
        }

        val applicationElements = roundEnv?.getElementsAnnotatedWith(Application::class.java)
        val handlerElements = roundEnv?.getElementsAnnotatedWith(RouteHandler::class.java)
        val pathElements = roundEnv?.getElementsAnnotatedWith(Path::class.java)
        val bindedElements = roundEnv?.getElementsAnnotatedWith(BindTo::class.java)
        val componentElements = roundEnv?.getElementsAnnotatedWith(Component::class.java)

        mapOf(
                handlerElements to handlers,
                pathElements to paths,
                componentElements to components,
                bindedElements to binded
        ).forEach { entry ->
            entry.key?.let { entry.value.addAll(it) }
        }

        applicationElements?.forEach { applicationElement ->
            val type = (applicationElement as TypeElement).asClassName()
            val vertx = ClassName("io.vertx.core", "Vertx")
            val pkg = type.packageName()

            val file = FileSpec.builder(pkg, type.simpleName() + "Main")
                    .addType(TypeSpec.objectBuilder(type.simpleName() + "Main")
                            .addFunction(FunSpec.builder("main")
                                    .addAnnotation(ClassName("", "JvmStatic"))
                                    .addParameter("args", String::class, KModifier.VARARG)
                                    .apply {
                                        val daggerComponentName = "Dagger${type.simpleName()}Component"

                                        if (components.isNotEmpty()) {
                                            addStatement("val d = $daggerComponentName.create()")
                                            addStatement("d.vertx().deployVerticle(d.verticle())")
                                        }
                                    }
                                    .build())
                            .build())
            file.build().writeTo(File(kaptKotlinGeneratedDir))

            val router = FileSpec.builder(pkg, type.simpleName() + "Router").apply {
                addStaticImport("io.vertx.core.http", "HttpServer")
                addStaticImport("io.vertx.kotlin.core.http", "HttpServerOptions")
            }
            router.addType(TypeSpec.classBuilder(type.simpleName() + "Router")
                    .constuctorProperties(
                            mkparameter("router", ClassName.bestGuess("io.vertx.ext.web.Router")).build(),
                            *(paths + handlers).map { it.enclosingElement }
                                    .toSet()
                                    .map { mkparameter(it.injectedFieldName, it.asType().asTypeName()).build()
                                    }.toTypedArray()) {
                        addAnnotation(Inject::class)
                    }
                    .addFunction(FunSpec.builder("start")
                            .addModifiers(KModifier.OVERRIDE)
                            .addCode(StringBuilder().apply {
                                appendln("super.start()")
                                handlers.mapNotNull { it as? ExecutableElement }.forEach {
                                    val order = it.getAnnotationsByType(Order::class.java).firstOrNull()
                                    val onroute = it.getAnnotationsByType(OnRoute::class.java).firstOrNull()?.code.let { if(it?.isNotBlank() == true) ".apply { $it }" else "" }
                                    val path = it.getAnnotationsByType(RouteHandler::class.java).firstOrNull()?.path.let { p(it); if(it?.isNotBlank() == true) "\"$it\"" else "" }
                                    appendln(CodeBlock.of("router.route($path)$onroute${ order?.p?.let { ".order($it)" } ?: "" }.handler(%L.%L)", it.enclosingElement.injectedFieldName, it.asFunctionName))
                                }
                                paths.forEach { it ->
                                    var path = it.getAnnotation(Path::class.java).value

                                    val onroute = it.getAnnotationsByType(OnRoute::class.java).firstOrNull()?.code.let { if(it?.isNotBlank() == true) ".apply { $it }" else "" }

                                    val routerMethod = when {
                                        it.getAnnotation(GET::class.java) != null -> "get"
                                        it.getAnnotation(POST::class.java) != null -> "post"
                                        else -> "route"
                                    }

                                    val parentElement = it.enclosingElement
                                    val parentPath = parentElement.getAnnotationsByType(ApplicationPath::class.java)
                                            .firstOrNull()?.value

                                    val order = it.getAnnotationsByType(Order::class.java).firstOrNull()

                                    val parameters = (it as? ExecutableElement)?.parameters ?: emptyList()
                                    val returnType = (it as? ExecutableElement)?.returnType
                                    val suspending = parameters.find { it.isCoroutineContinuation } != null
                                    if (suspending) coroutinesEnabled = true

                                    val directFunctionReference = parameters.size == 1 && parameters[0].asType()
                                            .toString() == "io.vertx.ext.web.RoutingContext"

                                    val args = parameters.filterNot { it.isCoroutineContinuation }
                                            .map { element ->
                                                when (element.asType().toString()) {
                                                    "io.vertx.ext.web.RoutingContext" -> CodeBlock.of("context")
                                                    "io.vertx.ext.web.Route" -> CodeBlock.of("context.currentRoute()")
                                                    "java.lang.String" -> CodeBlock.of("context.pathParam(%S)", element.simpleName)
                                                    "io.vertx.ext.web.Cookie" -> CodeBlock.of("context.getCookie(%S)", element.simpleName)
                                                    "java.util.Set<? extends io.vertx.ext.web.FileUpload>" -> CodeBlock.of("context.fileUploads()")
                                                    else -> CodeBlock.of("null")
                                                }
                                            }.joinToString(", ")

                                    val handlerMethod = when {
                                        //suspending -> "coroutineHandler"
                                        else -> "handler"
                                    }

                                    val functionCall = if (directFunctionReference) "%L::%L" else {
                                        when (returnType?.toString()) {
                                            java.lang.String::class.qualifiedName -> "context.response().end(%L.%L($args))"
                                            String::class.qualifiedName -> "context.response().end(%L.%L($args))"
                                            "java.lang.String" -> "context.response().end(%L.%L($args))"
                                            "java.io.File" -> "context.response().sendFile(%L.%L($args).absolutePath)"
                                            else -> "%L.%L($args)" }
                                                .let { "try { $it } catch (e: Exception) { context.fail(e) }" }
                                                .let {
                                                    if(suspending)
                                                        "{ context -> launch(vertx.dispatcher()) { $it } }"
                                                    else
                                                        "{ context -> $it }"
                                                }
                                    }

                                    parentPath?.let { path = "$parentPath$path" }
                                    appendln(CodeBlock.of("router.$routerMethod(%S)$onroute${ order?.p?.let { ".order($it)" } ?: "" }.$handlerMethod($functionCall) // %L %L",
                                            path, it.enclosingElement.injectedFieldName, it.simpleName, it.javaClass, it.annotationMirrors))
                                }
                                if (coroutinesEnabled) appendln("awaitResult<HttpServer> {")
                                appendln("vertx.createHttpServer(HttpServerOptions(logActivity = true))")
                                appendln("\t.requestHandler(router::accept)")
                                appendln("\t.listen($port, it).apply {")
                                appendln("\t\tprintln(\"Listening on port: \${actualPort()}\")")
                                appendln("}")
                                if (coroutinesEnabled) appendln("}")
                            }.let { CodeBlock.of(it.toString()) })
                            .apply { if (coroutinesEnabled) addModifiers(KModifier.SUSPEND) }
                            .build())
                    .superclass(ClassName.bestGuess(
                            if (coroutinesEnabled) "io.vertx.kotlin.coroutines.CoroutineVerticle"
                            else "io.vertx.core.AbstractVerticle"))
                    .build())

            if (coroutinesEnabled) {
                router.addStaticImport("io.vertx.kotlin.coroutines", "CoroutineVerticle")
                router.addStaticImport("io.vertx.kotlin.coroutines", "dispatcher")
                router.addStaticImport("io.vertx.kotlin.coroutines", "awaitResult")
                router.addStaticImport("kotlinx.coroutines.experimental", "launch")
            }

            router.build().writeTo(File(kaptKotlinGeneratedDir))

            val module = FileSpec.builder(pkg, type.simpleName() + "Module")
            module.addType(TypeSpec.classBuilder(type.simpleName() + "Module")
                    .addAnnotation(Module::class)
                    .addModifiers(KModifier.ABSTRACT)
                    .companionObject(TypeSpec.companionObjectBuilder(type.simpleName() + "Module")
                            .addFunction(FunSpec.builder("vertx")
                                    .addAnnotation(JvmStatic::class)
                                    .addAnnotation(Singleton::class)
                                    .addAnnotation(Provides::class)
                                    .returns(vertx)
                                    .addStatement("return Vertx.vertx()").build())
                            .addFunctionIf(false, FunSpec.builder("coroutineDispatcher")
                                    .addParameter("vertx", vertx)
                                    .addAnnotation(JvmStatic::class)
                                    .addAnnotation(Singleton::class)
                                    .addAnnotation(Provides::class)
                                    .returns(ClassName("kotlinx.coroutines.experimental", "CoroutineDispatcher"))
                                    .addStatement("return vertx.dispatcher()")
                                    .build())
                            .addFunction(FunSpec.builder("router")
                                    .addParameter("vertx", vertx)
                                    .addAnnotation(JvmStatic::class)
                                    .addAnnotation(Singleton::class)
                                    .addAnnotation(Provides::class)
                                    .returns(Class.forName("io.vertx.ext.web.Router"))
                                    .addStatement("return Router.router(vertx)").build())
                            .addFunction(FunSpec.builder("cookieHandler")
                                    .addAnnotation(JvmStatic::class)
                                    .addAnnotation(Singleton::class)
                                    .addAnnotation(Provides::class)
                                    .returns(Class.forName("io.vertx.ext.web.handler.CookieHandler"))
                                    .addStatement("return CookieHandler.create()").build())
                            .build())
                    .apply {
                        binded.forEach { element ->
                            val bindAnnotation = element.getAnnotation(BindTo::class.java)
                            val injectAs = bindAnnotation.catchTypeMirror { it.injectAs }
                            addFunction(FunSpec.builder("provide_binded_${element.injectedFieldName}")
                                    .addAnnotation(Binds::class)
                                    .addModifiers(KModifier.ABSTRACT)
                                    .addParameter(mkparameter("implementation", element.typeName).build())
                                    .returns(if (Unit::class == injectAs) ClassName.bestGuess(bindAnnotation.className)
                                    else injectAs.asTypeName())
                                    .build())
                        }
                    }
                    .addFunction(FunSpec.builder("verticle")
                            .addModifiers(KModifier.ABSTRACT)
                            .addParameter(mkparameter("_verticle", type.peerClass("${type.simpleName()}Router")).build())
                            .addAnnotation(Singleton::class)
                            .addAnnotation(Binds::class)
                            .returns(ClassName.bestGuess("io.vertx.core.Verticle"))
                            .build())
                    .build())
            module.build().writeTo(File(kaptKotlinGeneratedDir))

            val component = FileSpec.builder(pkg, type.simpleName() + "Component")
            component.addType(TypeSpec.interfaceBuilder(type.simpleName() + "Component")
                    .addAnnotation(Singleton::class)
                    .addAnnotation(
                            AnnotationSpec.builder(Component::class)
                                    .addMember("modules = arrayOf(${type.simpleName() + "Module"}::class)")
                                    .build())
                    .addFunction(FunSpec.builder("vertx").returns(vertx).addModifiers(KModifier.ABSTRACT).build())
                    .addFunction(FunSpec.builder("router").returns(ClassName.bestGuess("io.vertx.ext.web.Router")).addModifiers(KModifier.ABSTRACT).build())
                    .addFunction(FunSpec.builder("verticle").returns(ClassName.bestGuess("io.vertx.core.Verticle")).addModifiers(KModifier.ABSTRACT).build())
                    .build())
            component.build().writeTo(File(kaptKotlinGeneratedDir))

        }

        return components.isEmpty()
    }
}

fun <T : Annotation> T.catchTypeMirror(fn: (a: T) -> Unit): TypeMirror {
    try {
        fn(this)
    } catch (e: MirroredTypeException) {
        return e.typeMirror
    }
    throw RuntimeException("For some reason there wasn't a MirroredTypeException")
}

fun mkparameter(name: String, typeName: TypeName) = ParameterSpec.builder(name, typeName)

val ClassName.defaultConstructor: String
    get() = "${simpleName()}()"
val Element.injectedFieldName: String
    get() = "_" + (this as TypeElement).qualifiedName.str.replace(".", "_")
val ExecutableElement.asFunctionName: String
    get() = this.simpleName.str.removeSuffix("\$annotations")
val Element.isCoroutineContinuation: Boolean
    get() = asType().toString()
            .startsWith("kotlin.coroutines.experimental.Continuation")
val Element.typeName: TypeName
    get() = asType().asTypeName()

fun TypeSpec.Builder.constuctorProperties(vararg params: ParameterSpec, fn: FunSpec.Builder.() -> Unit = {}) = this.apply {
    primaryConstructor(FunSpec.constructorBuilder().apply {
        params.forEach { addParameter(it) }
        fn()
    }.build())
    params.forEach {
        addProperty(PropertySpec.builder(it.name, it.type)
                .initializer(it.name).build())
    }
}
fun TypeSpec.Builder.addFunctionIf(condition: Boolean, fn: FunSpec): TypeSpec.Builder = this.also { if (condition) addFunction(fn) }
fun <T> T.applyIf(condition: Boolean, fn: T.()->T) = if (condition) this.fn() else this
