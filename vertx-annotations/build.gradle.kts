import com.jfrog.bintray.gradle.BintrayExtension
import nl.javadude.gradle.plugins.license.LicenseExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm")
    id("com.jfrog.bintray") version "1.7.3"
    id("com.github.hierynomus.license") version "0.14.0"
    `maven-publish`
}

group = "lol.jtl"
version = "0.0.1"

apply {
    plugin("kotlin")
}

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib"))
    compile("io.vertx:vertx-lang-kotlin-coroutines:3.5.1")
    compile("com.squareup:kotlinpoet:0.7.0")
    compile("javax.ws.rs:jsr311-api:+")
    compile("com.google.dagger:dagger:2.+")
    //compile("com.google.dagger:dagger-compiler:2.x")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.6"
}

val bitbucketBuildNumber = System.getenv("BITBUCKET_BUILD_NUMBER") ?: ""
val publishReleaseVersion = bitbucketBuildNumber.isNotEmpty()

if(bitbucketBuildNumber.isNotEmpty()) {
    version = "$version-$bitbucketBuildNumber"
} else {
    version = "$version-dev"
}

publishing {
    repositories {
        maven {
            // change to point to your repo, e.g. http://my.org/repo
            mavenLocal()
        }
    }
    (publications) {
        "mavenJava"(MavenPublication::class) {
            from(components["java"])
            //artifact(sourcesJar)
        }
    }
}

configure<LicenseExtension> {
}

configure<com.jfrog.bintray.gradle.BintrayExtension> {
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_KEY")
    dryRun = System.getenv("DRYRUN") == "true"

    publish = publishReleaseVersion

    pkg(closureOf<BintrayExtension.PackageConfig> {
        repo = "vertx-annotations"
        name = "vertx-annotations"
        setLicenses("MPL-2.0")
        vcsUrl = "https://bitbucket.org/jtl/vertx-annotations"
    })

    setPublications("mavenJava")
}

