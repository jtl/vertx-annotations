import com.jfrog.bintray.gradle.BintrayExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.Date

plugins {
    val kotlinVersion = "1.2.31"
    kotlin("jvm") version kotlinVersion
    kotlin("kapt") version kotlinVersion
    id("com.github.ben-manes.versions") version "0.17.0"
    id("com.jfrog.bintray") version "1.7.3"
    id("com.github.hierynomus.license") version "0.14.0"
    `maven-publish`
}

val publishedProjects = listOf("compiler", "annotations")
tasks {
    "uploadAll" {
        dependsOn(*publishedProjects.map {":$it:bintrayUpload"}.toTypedArray())
    }
}

val bitbucketBuildNumber = System.getenv("BITBUCKET_BUILD_NUMBER") ?: ""
val publishReleaseVersion = bitbucketBuildNumber.isNotEmpty()

group = "lol.jtl"
version = "0.0.1"

version = if(bitbucketBuildNumber.isNotEmpty()) {
    "$version-$bitbucketBuildNumber"
} else {
    "$version-dev-${SimpleDateFormat("yyMMddHHmmss").format(Date())}"
}

allprojects.forEach { p -> if (publishedProjects.contains(p.name)) p.afterEvaluate {
    version = rootProject.version
    group = rootProject.group

    val repoName = name

    println("Publication name: $repoName")

    repositories {
        jcenter()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.6"
    }

    publishing {
        repositories {
            maven {
                mavenLocal()
            }
        }
        (publications) {
            (repoName)(MavenPublication::class) {
                from(components["java"])
            }
        }
    }

    configure<BintrayExtension> {
        user = System.getenv("BINTRAY_USER")
        key = System.getenv("BINTRAY_KEY")
        dryRun = System.getenv("BINTRAY_DRYRUN") == "true"

        override = System.getenv("BINTRAY_OVERRIDE") == "true"

        publish = System.getenv("BINTRAY_PUBLISH") == "true"

        pkg(closureOf<BintrayExtension.PackageConfig> {
            repo = "vertx-annotations"
            name = "vertx-$repoName"
            setLicenses("MPL-2.0")
            vcsUrl = "https://bitbucket.org/jtl/vertx-annotations"
        })

        setPublications(repoName)
    }
} }