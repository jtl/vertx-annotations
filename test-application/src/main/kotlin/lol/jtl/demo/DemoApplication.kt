package lol.jtl.demo

import io.vertx.ext.web.RoutingContext
import lol.jtl.annotations.*
import javax.inject.Inject
import javax.ws.rs.*

@Application
class DemoApplication @Inject constructor() {

    @GET
    @Path("/")
    fun get() = "Hello"

    @POST
    @Path("/test")
    suspend fun post(context: RoutingContext) {
        context.response().setStatusCode(404).end("I'm not here.")
    }

}
