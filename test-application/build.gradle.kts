import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.kotlin.gradle.dsl.Coroutines
import org.jetbrains.kotlin.gradle.internal.KaptGenerateStubsTask
import org.jetbrains.kotlin.gradle.internal.KaptTask
import org.jetbrains.kotlin.gradle.plugin.KaptExtension
import org.jetbrains.kotlin.gradle.tasks.KaptOptions

plugins {
    application
    kotlin("jvm")
    kotlin("kapt")
}

group = "lol.jtl"
version = "0.0.1-SNAPSHOT"

application {
    mainClassName = "lol.jtl.demo.DemoApplicationMain"
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
    this.copyClassesToJavaOutput = true
}

val deployDir = File(project.buildDir, "heroku").apply { mkdirs() }
val deployUrl = "https://git.heroku.com/vast-brushlands-44060.git"

tasks {
    withType<KotlinCompile> {
        //source(project.files("build/generated/source/kapt/main"),
        //        project.files("build/generated/source/kaptKotlin/main"))
        kotlinOptions {
            jvmTarget = "1.8"
            freeCompilerArgs = listOf("-Xjsr305=strict")
        }
    }
}

java.sourceSets["main"].java {
    srcDir(file("build/generated/source/kaptKotlin/main"))
    srcDir(file("build/generated/source/kapt/main"))
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    kapt(project(":compiler"))
    compile(project(":annotations"))
    compile(kotlin("stdlib-jdk8"))
    compile(kotlinx("kotlinx-html-jvm", "0.6.9"))
    compile("io.vertx:vertx-lang-kotlin:3.5.1")
    compile("io.vertx:vertx-lang-kotlin-coroutines:3.5.1")
    compile("io.vertx:vertx-web:3.5.1")
    compile("io.vertx:vertx-core:3.5.1")
    compile("io.vertx:vertx-jdbc-client:3.5.1")
    compile("io.vertx:vertx-auth-jwt:3.5.1")
    compile("org.hsqldb:hsqldb:2.3.4")
    compile("org.slf4j:slf4j-jdk14:1.7.21")
    compile("javax.ws.rs:jsr311-api:1.1.1")
    compileOnly("com.google.dagger:dagger:2.+")
    kapt("com.google.dagger:dagger-compiler:2.+")
}

fun DependencyHandlerScope.kotlinx(module: String, version: String): Any =
        "org.jetbrains.kotlinx:$module:$version"
